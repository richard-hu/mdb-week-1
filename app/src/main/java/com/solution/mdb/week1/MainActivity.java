package com.solution.mdb.week1;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    Button flowButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        flowButton = (Button) findViewById(R.id.flow_button);
        loadStartButton(this);
    }

    private void loadStartButton(final Context context) {
        flowButton.setText("START");
        flowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent launchGame = new Intent(context, GameActivity.class);
                startActivity(launchGame);
            }
        });
    }
}
