package com.solution.mdb.week1;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.TreeSet;

/**
 * Created by Richard on 9/17/2016.
 */
public class GameActivity extends AppCompatActivity {
    // stores the image resource IDs to put in the ImageView with names as keys
    static HashMap<String, Integer> NAME_TO_RESOURCE_ID = new HashMap<>();

    // the list of all of our names. :)
    static String[] NAME_BANK = {"Jessica Cherny", "Mansi Shah", "Alice Wang", "Jessica Ji", "Kevin Jiang", "Jared Gutierrez", "Kristin Ho", "Christine Munar", "Mudit Mittal", "Ali Shelton", "Richard Hu", "Shaan Appel", "Ankur Mahesh", "Edward Liu", "Wilbur Shi", "Young Lin", "Abhinav Koppu", "Abhishek Mangla", "Akkshay Khoslaa", "Ally Koo", "Andy Wang", "Aneesh Jindal", "Anisha Salunkhe", "Aparna Krishnan", "Ashwin Vaidyanathan", "Cody Hsieh", "Connor Killion", "Jeffrey Zhang", "Justin Kim", "Kenneth Steele", "Krishnan Rajiyah", "Lisa Lee", "Peter Schafhalter", "Riley Edmunds", "Rohan Narayan", "Sahil Lamba", "Sameer Suresh", "Sona Jeswani", "Sirjan Kafle", "Tarun Khasnavis", "Virindh Borra", "Billy Lu", "Aayush Tyagi", "Ben Goldberg", "Candice Ye", "Eliot Han", "Emaan Hariri", "Jessica Chen", "Katharine Jiang", "Kedar Thakkar", "Leon Kwak", "Mohit Katyal", "Rochelle Shen", "Sayan Paul", "Sharie Wang", "Shreya Reddy", "Shubham Goenka", "Victor Sun", "Vidya Ravikumar"};

    // some encouraging phrases to show when you get an answer right
    static String[] RIGHT_ANSWER = {"Great job!", "You got it!", "Nice work!", "You're doing great!", "Congrats, that's correct!"};

    // you suck (jk), for when you get an answer wrong
    static String[] WRONG_ANSWER = {"Not quite.", "Almost.", "Try again.", "Close!"};

    // the current context, to pass into listeners and handlers and stuff
    GameActivity context = this;
    // used to do stuff after a timed delay
    Handler handler = new Handler();
    // the timer on screen
    CountDownTimer timer;
    // the 4 buttons with guesses on them
    Button guessButton1, guessButton2, guessButton3, guessButton4, quitButton;
    // the text on the bottom with score and time
    TextView countdownTextView, scoreTextView;
    // the ImageView for the chosen person
    ImageView portraitImageView;
    // the chosen person's name
    String activeAnswer = "harambe";
    // your points!
    int gameScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        initialize();
        startNewRound();
    }

    // compares two name Strings, consistent across NAME_BANK format and image name format
    private boolean nameEquals(String name1, String name2) {
        String format1 = name1.replace(" ", "");
        String format2 = name2.replace(" ", "");
        return format1.equalsIgnoreCase(format2);
    }

    // load the necessary resources for the first time
    private void initialize() {
        loadResourceMap();
        loadViews();
        loadOnClickListeners();
        setDefaultText();
    }

    // set the text fields with default text
    private void setDefaultText() {
        gameScore = 0;
        scoreTextView.setText("Score: " + gameScore);
    }

    // fill the map for accessing people's pictures by name
    private void loadResourceMap() {
        for (String name : NAME_BANK) {
            String formatted = name.toLowerCase().replace(" ", "");
            int resourceId = getResources().getIdentifier(formatted, "drawable", getPackageName());
            NAME_TO_RESOURCE_ID.put(name, resourceId);
        }
    }

    // populate the view variables up top
    private void loadViews() {
        guessButton1 = (Button) findViewById(R.id.guess_button_1);
        guessButton2 = (Button) findViewById(R.id.guess_button_2);
        guessButton3 = (Button) findViewById(R.id.guess_button_3);
        guessButton4 = (Button) findViewById(R.id.guess_button_4);
        quitButton = (Button) findViewById(R.id.quit_button);
        scoreTextView = (TextView) findViewById(R.id.score_text_view);
        countdownTextView = (TextView) findViewById(R.id.countdown_text_view);
        portraitImageView = (ImageView) findViewById(R.id.portrait_image);

        // add 2 extra ticks, 1 to make it start at 5 instead of 4 and 1 to add an "Out of time!"
        // message
        timer = new CountDownTimer(7000, 1000) {
            public void onTick(long millisUntilFinished) {
                if (millisUntilFinished < 2000) {
                    countdownTextView.setText("Out of time!");

                } else {
                    countdownTextView.setText("Time left: " + (millisUntilFinished / 1000 - 1));
                }
            }

            public void onFinish() {
                startNewRound();
            }
        };
    }

    // make it do stuff when you click on buttons
    private void loadOnClickListeners() {
        loadOnClickListener(guessButton1);
        loadOnClickListener(guessButton2);
        loadOnClickListener(guessButton3);
        loadOnClickListener(guessButton4);
        quitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // build-your-own AlertDialog
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

                alertDialogBuilder.setTitle("Do you want to quit?");
                alertDialogBuilder
                        .setMessage("Once you quit, you'll lose your score!")
                        .setCancelable(false)
                        .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                context.finish();
                                Intent restartIntent = new Intent(GameActivity.this, MainActivity.class);
                                startActivity(restartIntent);
                            }
                        })
                        .setNegativeButton("No",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        // make the image clickable to open contacts
        portraitImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // the add a contact common intent
                Intent intent = new Intent(Intent.ACTION_INSERT);
                intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
                intent.putExtra(ContactsContract.Intents.Insert.NAME, activeAnswer);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });
    }

    // for loading the on click listener for the buttons; since all of the buttons do basically the
    // same thing, putting it in a separate method saves some copypasta
    private void loadOnClickListener(final Button button) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.cancel();
                String message;
                if (nameEquals(button.getText().toString(), activeAnswer)) {
                    incrementScore();
                    message = RIGHT_ANSWER[(int) (Math.random() * RIGHT_ANSWER.length)];
                } else {
                    message = WRONG_ANSWER[(int) (Math.random() * WRONG_ANSWER.length)];
                }
                // tell the player if they were right or not, and re-start the round after
                Toast.makeText(GameActivity.this, message, Toast.LENGTH_SHORT).show();
                // remove any other handler callbacks first in case they're still waiting
                handler.removeCallbacksAndMessages(null);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startNewRound();
                    }
                }, Toast.LENGTH_SHORT);
            }
        });
    }

    // start a new round regardless of where you are in the round
    private void startNewRound() {
        pickNamesAndPicture();
        timer.start();
    }

    // pick the names, the answer, and the picture for the answer
    private void pickNamesAndPicture() {
        TreeSet<Integer> nameIndexes = new TreeSet<>();
        while (nameIndexes.size() < 4) {
            nameIndexes.add((int) (Math.random() * NAME_BANK.length));
        }
        Integer[] indexArray = new Integer[4];
        indexArray = nameIndexes.toArray(indexArray);

        guessButton1.setText(NAME_BANK[indexArray[0]]);
        guessButton2.setText(NAME_BANK[indexArray[1]]);
        guessButton3.setText(NAME_BANK[indexArray[2]]);
        guessButton4.setText(NAME_BANK[indexArray[3]]);
        theChosenOne(indexArray);
    }

    // pick a candidate from the 4, and load their picture and name
    private void theChosenOne(Integer[] candidates) {
        activeAnswer = NAME_BANK[candidates[(int) (Math.random() * candidates.length)]];
        // trash the old picture to save memory
        // ((BitmapDrawable) portraitImageView.getDrawable()).getBitmap().recycle();
        if (NAME_TO_RESOURCE_ID.containsKey(activeAnswer)) {
            portraitImageView.setImageResource(NAME_TO_RESOURCE_ID.get(activeAnswer));
        } else {
            portraitImageView.setImageResource(R.drawable.harambe);
        }
    }

    // update score and text
    private void incrementScore() {
        gameScore++;
        scoreTextView.setText("Score: " + gameScore);
    }
}
